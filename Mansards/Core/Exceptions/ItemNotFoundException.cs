﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Exceptions
{
    using System.Runtime.CompilerServices;

    public class ItemNotFoundException:Exception
    {
        public ItemNotFoundException(string message)
            : base(message)
        {
        }
    }
}
