﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Builders
{
    using System.Net.Mail;
    using System.Runtime.CompilerServices;

    public class EmailBuilder: IEmailBuilder
    {
        private MailMessage message;

        public EmailBuilder CreateMessage()
        {
            this.message = new MailMessage();
            return this;
        }

        public EmailBuilder WithRecipient(string recipients)
        {
            MailAddressCollection mails = new MailAddressCollection();
            this.message.To.Add(recipients);
            return this;
        }

        public EmailBuilder WithSubject(string subject)
        {
            this.message.Subject = subject;
            return this;
        }

        public EmailBuilder WithBody(string messageBody)
        {
            this.message.Body = messageBody;
            return this;
        }

        public EmailBuilder WithSender(string address)
        {
            this.message.Sender =  EMailBuilderFactory.Get<MailAddressBuilder>()
                .Create(address)
                .Build();

            return this;
        }

        public EmailBuilder WithFrom(string address)
        {
            this.message.From = EMailBuilderFactory.Get<MailAddressBuilder>()
                .Create(address)
                .Build();

            return this;
        }

        public MailMessage Build()
        {
            return this.message;
        }
    }
}
