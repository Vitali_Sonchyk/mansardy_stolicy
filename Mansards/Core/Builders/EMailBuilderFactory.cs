﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Builders
{
    public class EMailBuilderFactory
    {
        public static TBuilder Get<TBuilder>() where TBuilder : IEmailBuilder
        {
            return Activator.CreateInstance<TBuilder>();
        }
    }
}
