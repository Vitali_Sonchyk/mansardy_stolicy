﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Builders
{
    using System.Net.Mail;

    public class MailAddressBuilder:IMailAddressBuilder
    {
        private MailAddress mailAddress;

        public MailAddressBuilder Create(String address)
        {
            this.mailAddress = new MailAddress(address);
            return this;
        }

        public MailAddress Build()
        {
            return this.mailAddress;
        }
    }
}
