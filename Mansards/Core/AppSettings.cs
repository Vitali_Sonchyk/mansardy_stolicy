﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Core
{
    using System.Runtime.CompilerServices;

    public static class AppSettings
    {
        private static string purchaseEmailRecipient;
        private static string purchaseEmailHost;
        private static string purchaseEmailPort;
        private static string purchaseEmailSender;
        private static string purchaseEmailSenderPassword;

        static AppSettings()
        {
            Init();
        }
        public static string PurchaseEmailRecipient
        {
            get
            {
                return purchaseEmailRecipient;
            }
        }

        public static string PurchaseEmailHost
        {
            get
            {
                return purchaseEmailHost;
            }
        }

        public static int PurchaseEmailPort
        {
            get
            {
                return Int32.Parse(purchaseEmailPort);
            }
            
        }

        public static string PurchaseEmailSender
        {
            get
            {
                return purchaseEmailSender;
            }
        }

        public static string PurchaseEmailSenderPassword
        {
            get
            {
                return purchaseEmailSenderPassword;
            }
        }
        private static void Init()
        {
            purchaseEmailRecipient = WebConfigurationManager.AppSettings["purchaseEmailRecipient"];
            purchaseEmailHost= WebConfigurationManager.AppSettings["emailSmtpHost"];
            purchaseEmailPort = WebConfigurationManager.AppSettings["emailSmtpPort"];
            purchaseEmailSender = WebConfigurationManager.AppSettings["purchaseEmailSender"];
            purchaseEmailSenderPassword = WebConfigurationManager.AppSettings["purchaseEmailSenderPassword"];
        }
    }
}
