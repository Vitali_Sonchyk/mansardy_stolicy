﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Email
{
    using System.Net;
    using System.Net.Mail;

    public class EmailClient
    {
        private MailAddress senderAddress;
        private SmtpClient client;

        public EmailClient()
        {
            this.senderAddress = new MailAddress(AppSettings.PurchaseEmailSender);
        }

        public SmtpClient Client
        {
            get
            {
                if (this.client == null)
                {
                    this.Init();
                }
                return this.client;
            }
        }

        public SuccessMailSentModel SendMail(MailMessage message)
        {
            SuccessMailSentModel successModel = new SuccessMailSentModel {Success = true};

            try
            {
                this.Client.Send(message);
            }

            catch (Exception ex)
            {
                successModel.Success = false;
            }
            
            return successModel;
        }

        private SmtpClient Init()
        {
            this.client = new SmtpClient();
            this.client.Host = AppSettings.PurchaseEmailHost;
            this.client.Port = AppSettings.PurchaseEmailPort;
            this.client.EnableSsl = true;
            this.client.DeliveryMethod = SmtpDeliveryMethod.Network;
            this.client.UseDefaultCredentials = false;
            this.client.Credentials = new NetworkCredential(
                this.senderAddress.Address,
                AppSettings.PurchaseEmailSenderPassword);

            return this.client;
        }
    }
}
