﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BI.BusinessModels
{
    public class EmailBusinessModel: IBusinessModel
    {
        public string SenderName
        {
            get;
            set;
        }

        public string SenderEmail
        {
            get;
            set;
        }

        public string SenderPhone
        {
            get;
            set;
        }

        public string SenderComment
        {
            get;
            set;
        }
    }
}
