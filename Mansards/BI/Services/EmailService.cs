﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BI.Services
{
    using System.Net.Mail;
    using System.Runtime.Remoting.Channels;

    using BI.BusinessModels;

    using Core.Builders;
    using System.Web.Configuration;

    using BI.Email;

    using Core;
    using Core.Email;

    class EmailService:IEmailService
    {
        private EmailClient emailClient;

        public EmailService()
        {
            this.emailClient = new EmailClient();
        }

        public SuccessMailSentModel SendMail(EmailBusinessModel model)
        {
            MailMessage message = EmailFactory.CreateEmailMessage(model);
            return this.emailClient.SendMail(message);
        }
    }
}
