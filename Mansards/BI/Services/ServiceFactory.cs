﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BI.Services
{
    using Core.Exceptions;

    public static class ServiceFactory
    {
        private static Dictionary<Type, Func<IService>> serviceDict = new Dictionary<Type, Func<IService>>
        {
            {typeof(IEmailService), () => new EmailService() }
        };

        public static T Get<T>() where T: IService
        {
            Func<IService> func;

            if(!serviceDict.TryGetValue(typeof(T), out func))
            {
                throw new ItemNotFoundException(String.Format("The realization of {0} interface was not found!", typeof(T)));
            }

            return (T)func.Invoke();
        }
    }
}
