﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BI.Services
{
    using BI.BusinessModels;

    using Core.Email;

    public interface IEmailService:IService
    {
        SuccessMailSentModel SendMail(EmailBusinessModel model);
    }
}
