﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BI.Email
{
    using System.Net.Mail;

    using BI.BusinessModels;

    using Core;
    using Core.Builders;

    class EmailFactory
    {
        public static MailMessage CreateEmailMessage(EmailBusinessModel model)
        {
            MailMessage message =
                EMailBuilderFactory.Get<EmailBuilder>()
                    .CreateMessage()
                    .WithRecipient(AppSettings.PurchaseEmailRecipient)
                    .WithSubject(String.Format("Заявка на строительство"))
                    .WithSender(AppSettings.PurchaseEmailSender)
                    .WithFrom(AppSettings.PurchaseEmailSender)
                    .WithBody(
                        String.Format(
                            "{0} подал заявку на сроительство. Контактная информация: \r\n Телефон :{1}. Почтовый адрес: {2}; \r\n Оставил комментарий: {3}",
                            model.SenderName,
                            model.SenderPhone,
                            model.SenderEmail,
                            model.SenderComment))
                    .Build();

            return message;
        }
    }
}
