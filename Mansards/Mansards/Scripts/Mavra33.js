﻿$(document).ready(function () {
    initializeGalary();
});

function collapseButtonsContent() {
    var $buttons = $(".collapse.in", "#collapse_container")
        $buttons.collapse('hide');
}

function initializeGalary() {
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
}
