﻿
$(document).ready(function () {
    initiateFormValidation();
    initiateFormSubmit();
    //initializeHidingOfElements();
});

function initiateFormValidation() {
    $("#purchaseForm").validate({
        rules: {
            purchaserName: {
                required: true
            },
            emailAddress: {
                email: true
            },
            phone: {
                required: true
            }
        },

        messages: {
            purchaserName: {
                required: "Введите имя"
            },
            emailAddress: "Пожалуйста, введите правильный почтовый адрес",
            phone: "Введите номер телефона"
        },

        errorClass: "errorFormHighlight"
    });
}

function initializeHidingOfElements() {
    $("[data-hide]").on("click", function () {
        $("." + $(this).attr("data-hide")).hide();
    });
}

function initiateFormSubmit() {
    $("#purchaseForm").on("submit", function (e) {
        e.preventDefault();
        tryPurchaseSubmit();
    });
}

function handleEmailSubmit(result) {
    if (result.Success) {
        clearFields();
        $("#successAlert").removeClass("hidden");


    } else {
        clearFields();
        $("#errorAllert").removeClass("hidden");
    }
}

function handleErrorPurchase() {
    clearFields();
    $("#errorAllert").removeClass("hidden");

}

function tryPurchaseSubmit() {
    var bValid = $("#purchaseForm").valid();
    if (bValid) {
        submitPurchaseRequest();
    }
}

function submitPurchaseRequest() {
    var dataObject = JSON.stringify({
        'SenderName': $('#purchaserName').val(),
        'SenderEmail': $('#emailAddress').val(),
        'SenderPhone': $('#phone').val(),
        'SenderComment': $('#comment').val()
    });

    $.ajax({
        url: '/api/Purchase/PerformPurchase',
        type: 'POST',
        contentType: 'application/json',
        data: dataObject,
        success: handleEmailSubmit,
        error: handleErrorPurchase
    });
}

function clearFields() {
    $('#purchaserName').val("");
    $('#emailAddress').val("");
    $('#phone').val("");
    $('#comment').val("");
}

function collapseButtonsContent() {
    var $buttons = $(".collapse.in", "#collapse_container")
    $buttons.collapse('hide');
}

function closeAlert() {
    $("#successAlert,#errorAllert").addClass("hidden");
}


