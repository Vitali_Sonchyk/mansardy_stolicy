﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mansards.Startup))]
namespace Mansards
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
