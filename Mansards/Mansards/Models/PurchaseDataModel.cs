﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mansards.Models
{
    public class PurchaseDataModel
    {
        public string Name
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }
    }
}