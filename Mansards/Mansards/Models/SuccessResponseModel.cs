﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mansards.Models
{
    public class SuccessResponseModel
    {
        public bool Success
        {
            get;
            set;
        }
    }
}