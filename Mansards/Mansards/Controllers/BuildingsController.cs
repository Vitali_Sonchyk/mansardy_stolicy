﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mansards.Controllers
{
    public class BuildingsController : Controller
    {
        // GET: Buildings
        public ActionResult Mavra29()
        {
            ViewBag.Title = "Я.Мавра 29";
            return View("Mavra29");
        }

        public ActionResult Mavra33()
        {
            ViewBag.Title = "Я.Мавра 33";
            return View("Mavra33");
        }

        public ActionResult Mavra31()
        {
            ViewBag.Title = "Я.Мавра 31";
            return View("Mavra31");
        }
    }
}