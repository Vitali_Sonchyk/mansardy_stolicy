﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mansards.Controllers
{
    using BI.BusinessModels;
    using BI.Services;
    using Core.Email;

    using Mansards.Models;

    public class PurchaseController : ApiController
    {
        [System.Web.Mvc.HttpPost]
        public IHttpActionResult PerformPurchase(EmailBusinessModel model)
        {
            SuccessMailSentModel mailSentModel = ServiceFactory.Get<IEmailService>().SendMail(model);
            return this.Ok(new SuccessResponseModel { Success = true });
        }
    }
}
